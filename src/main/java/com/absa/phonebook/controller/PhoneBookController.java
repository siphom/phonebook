package com.absa.phonebook.controller;

import com.absa.phonebook.dto.PersonDTO;
import com.absa.phonebook.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "${phonebook.app.api-prefix}/")
public class PhoneBookController {

    @Autowired
    private PersonService personService;

    @PostMapping("/person")
    @ResponseStatus(HttpStatus.CREATED)
    public PersonDTO create(@Validated @RequestBody PersonDTO dto)
    {
        return personService.create(dto);
    }

    @PutMapping("/person")
    @ResponseStatus(HttpStatus.CREATED)
    public PersonDTO update(@Validated @RequestBody PersonDTO dto){
        return personService.update(dto);
    }

    @GetMapping("/person/{id}")
    public PersonDTO searchByPersonId(@PathVariable(name = "id") Long id){
        return personService.search(id);
    }

    @GetMapping("/person/name/{name}")
    public List<PersonDTO> searchByName(@PathVariable(name = "name") String name){
        return personService.search(name);
    }

    @GetMapping("/person/all")
    public List<PersonDTO> searchAll(){
        return personService.findAll();
    }
}