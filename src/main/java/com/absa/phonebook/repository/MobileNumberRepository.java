package com.absa.phonebook.repository;

import com.absa.phonebook.domain.MobileNumber;
import com.absa.phonebook.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface MobileNumberRepository extends JpaRepository<MobileNumber, Long>, JpaSpecificationExecutor<MobileNumber>{
    List<MobileNumber> findByPerson(Long id);
}
