package com.absa.phonebook.repository;

import com.absa.phonebook.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long>, JpaSpecificationExecutor<Person>{
   /* List<Person> findByFirstName(String name);*/
    List<Person> findByFirstNameContaining(String name);
}
