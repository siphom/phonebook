package com.absa.phonebook.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

@Entity
@Table(name = "MOBILE_NUMBER", uniqueConstraints = @UniqueConstraint(columnNames = {"PERSON_ID", "MOBILE_NUMBER"}))
@Access(AccessType.FIELD)
@SequenceGenerator(name = "seq_absa", sequenceName = "seq_mobilenumber", allocationSize = 1, initialValue = 1)
public class MobileNumber extends AbstractPersistable<Long>{
    @Column(name = "MOBILE_NUMBER")
    private String mobileNumber;
    @Column(name = "PERSON_ID")
    private Long person;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Long getPerson() {
        return person;
    }

    public void setPerson(Long person) {
        this.person= person;
    }
}
