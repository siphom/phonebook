package com.absa.phonebook.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
@Entity
@Table(name = "person")
@Access(AccessType.FIELD)
@SequenceGenerator(name = "seq_absa", sequenceName = "seq_person", allocationSize = 1, initialValue = 1)
public class Person extends AbstractPersistable<Long>{
    private Long idNo;
    private String firstName;
    private String lastName;
    private String email;
    private String race;

    public Long getIdNo() {
        return idNo;
    }

    public void setIdNo(Long idNo) {
        this.idNo = idNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }
}
