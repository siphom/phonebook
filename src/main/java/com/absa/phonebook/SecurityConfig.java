package com.absa.phonebook;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * This class configures endpoints that should not be authenticated
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public SecurityConfig() {

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/health").permitAll()
                .antMatchers("/info").permitAll()
                .antMatchers("/trace").permitAll()
                .antMatchers("/mappings").permitAll()
                .antMatchers("/metrics").permitAll()


                .antMatchers("/v2/api-docs*/**").permitAll()
                .antMatchers("/swagger-resources*/**").permitAll()
                .antMatchers("/swagger-ui.html*/**").permitAll()
                .antMatchers("/swagger-ui.html/").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/webjars*/**").permitAll()

                .antMatchers("/api/person").permitAll()

                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/**").authenticated()
                .and()
                .headers().cacheControl();


    }
}
