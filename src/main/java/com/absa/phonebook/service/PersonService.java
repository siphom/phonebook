package com.absa.phonebook.service;

import com.absa.phonebook.ApplicationException;
import com.absa.phonebook.domain.MobileNumber;
import com.absa.phonebook.domain.Person;
import com.absa.phonebook.dto.MobileNumberDTO;
import com.absa.phonebook.dto.PersonDTO;
import com.absa.phonebook.repository.MobileNumberRepository;
import com.absa.phonebook.repository.PersonRepository;
import com.absa.phonebook.mapper.Mapper;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    @Autowired
    public PersonRepository personRepo;
    private Mapper mapper = new Mapper();

    @Autowired
    private MobileNumberRepository mobileNumberRepo;

    private static Logger logger = LoggerFactory.getLogger(PersonService.class);

    public PersonService(PersonRepository personRepo, MobileNumberRepository mobileNumberRepo){
        this.personRepo = personRepo;
        this.mobileNumberRepo = mobileNumberRepo;
    }


    @Transactional
    public PersonDTO create(PersonDTO dto){
        Person person = personRepo.save(mapper.toPerson(dto));
        List<MobileNumberDTO> mobileNumberDTOS = new ArrayList<>();
        dto.getMobileNumberList().forEach(mobileNumberDTO -> {
            MobileNumber mobileNumber = mapper.toMobileNumber(person, mobileNumberDTO.getMobileNumber());
            mobileNumberDTOS.add(mapper.toMobileNumber(mobileNumberRepo.save(mobileNumber)));
        });
        dto = mapper.toPersonDTO(person);
        dto.setMobileNumberList(mobileNumberDTOS);
        return dto;
    }

    @Transactional
    public PersonDTO update(PersonDTO dto){
        Person person = personRepo.findOne(dto.getId());
        if (person == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_PERSON_NOT_FOUND, "Person not found while attempting to update");
        }

        person = personRepo.save(mapper.toPerson(person, dto));
        List<MobileNumberDTO> mobileNumberDTOS = dto.getMobileNumberList();
        List<MobileNumber> mobileNumbers = mobileNumberRepo.findByPerson(person.getId());

        removePersonContact(mobileNumberDTOS, mobileNumbers);
        insertOrUpdate(person, mobileNumberDTOS);

        List<MobileNumberDTO> mobDTO = getMobileNumbers(person);
        dto = mapper.toPersonDTO(person);
        dto.setMobileNumberList(mobDTO);
        return dto;
    }

    @NotNull
    private List<MobileNumberDTO> getMobileNumbers(Person person) {
        List<MobileNumberDTO> mobDTO = new ArrayList<>();
        List<MobileNumber> numbers = mobileNumberRepo.findByPerson(person.getId());
        if(!numbers.isEmpty()){
            numbers.forEach(mobileNumber -> {
                mobDTO.add(mapper.toMobileNumber(mobileNumber));
            });
        }
        return mobDTO;
    }

    private void insertOrUpdate(Person person, List<MobileNumberDTO> mobileNumberDTOS) {
        if (!mobileNumberDTOS.isEmpty()){
            Person finalPerson = person;
            mobileNumberDTOS.forEach(mobileNumberDTO -> {
                if ((mobileNumberDTO.getId() == null || mobileNumberDTO.getId() == 0) && mobileNumberDTO.getMobileNumber() != null){
                    //new insert
                    mobileNumberRepo.save(mapper.toMobileNumber(finalPerson, mobileNumberDTO.getMobileNumber()));
                } else {
                    MobileNumber mob = mobileNumberRepo.findOne(mobileNumberDTO.getId());
                    if (mob != null){
                        //update
                        mobileNumberRepo.save(mapper.toMobileNumber(finalPerson, mob, mobileNumberDTO.getMobileNumber()));
                    }
                }
            });
        }
    }

    private void removePersonContact(List<MobileNumberDTO> mobileNumberDTOS, List<MobileNumber> mobileNumbers) {
        if (!mobileNumbers.isEmpty()){
            mobileNumbers.forEach(mobileNumber -> {
                final boolean[] delete = {true};
                if (!mobileNumberDTOS.isEmpty()){
                    mobileNumberDTOS.forEach(mobileNumberDTO -> {
                        if (mobileNumber.getId().equals(mobileNumberDTO.getId())){
                            delete[0] = false;
                        }
                    });
                    if (delete[0]){
                        //delete
                        mobileNumberRepo.delete(mobileNumber);
                    }
                }
            });
        }
    }

    @Transactional
    public PersonDTO search(Long id){
        Person person = personRepo.findOne(id);
        if (person == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_PERSON_NOT_FOUND, "Person not found");
        }
        PersonDTO personDTO = mapper.toPersonDTO(person);
        personDTO.setMobileNumberList(getMobileNumbers(person));
        return personDTO;
    }

    @Transactional
    public List<PersonDTO> search(String name){
        List<Person> listOfPerson = personRepo.findByFirstNameContaining(name);
        if (listOfPerson.isEmpty()){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_PERSON_NOT_FOUND, "Person not found");
        }
        List<PersonDTO> listOfPersonDTO = getPersonDTOS(listOfPerson);
        return listOfPersonDTO;
    }

    @Transactional
    public List<PersonDTO> findAll(){
        List<Person> listOfPerson = personRepo.findAll();
        List<PersonDTO> listOfPersonDTO = getPersonDTOS(listOfPerson);
        return listOfPersonDTO;
    }

    @NotNull
    private List<PersonDTO> getPersonDTOS(List<Person> listOfPerson) {
        List<PersonDTO> listOfPersonDTO = new ArrayList<>();
        listOfPerson.forEach(person -> {
            PersonDTO personDTO = mapper.toPersonDTO(person);
            personDTO.setMobileNumberList(getMobileNumbers(person));
            listOfPersonDTO.add(personDTO);
        });
        return listOfPersonDTO;
    }
}