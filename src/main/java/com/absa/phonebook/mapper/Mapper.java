package com.absa.phonebook.mapper;

import com.absa.phonebook.domain.MobileNumber;
import com.absa.phonebook.domain.Person;
import com.absa.phonebook.dto.MobileNumberDTO;
import com.absa.phonebook.dto.PersonDTO;

public class Mapper {

    public MobileNumber toMobileNumber(Person person, String mobileNumer){
        MobileNumber mobileNumber = new MobileNumber();
        mobileNumber.setPerson(person.getId());
        mobileNumber.setMobileNumber(mobileNumer);
        return mobileNumber;
    }
    public MobileNumber toMobileNumber(Person person, MobileNumber mobileNumber, String mobileNumer){
        mobileNumber.setMobileNumber(mobileNumer);
        return mobileNumber;
    }

    public MobileNumberDTO toMobileNumber(MobileNumber mobileNumber){
        MobileNumberDTO dto = new MobileNumberDTO();
        dto.setId(mobileNumber.getId());
        dto.setPersonId(mobileNumber.getPerson());
        dto.setMobileNumber(mobileNumber.getMobileNumber());
        return dto;
    }

    public Person toPerson(PersonDTO dto){
        Person person = new Person();
        setPerson(dto, person);
        return person;
    }

    public Person toPerson(Person emp, PersonDTO dto){
        setPerson(dto, emp);
        return emp;
    }

    private void setPerson(PersonDTO dto, Person person) {
        person.setIdNo(dto.getIdNo());
        person.setEmail(dto.getEmail());
        person.setFirstName(dto.getFirstName());
        person.setIdNo(dto.getIdNo());
        person.setRace(dto.getRace());
        person.setLastName(dto.getLastName());
    }

    public PersonDTO toPersonDTO(Person person){
        PersonDTO dto = new PersonDTO();
        if (person != null){
            dto.setEmail(person.getEmail());
            dto.setFirstName(person.getFirstName());
            dto.setIdNo(person.getIdNo());
            dto.setId(person.getId());
            dto.setRace(person.getRace());
            dto.setLastName(person.getLastName());
            dto.setId(person.getId());
        }
        return dto;
    }
}
